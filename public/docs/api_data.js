define({ "api": [
  {
    "type": "get",
    "url": "/chapter/:id",
    "title": "2- Show Chapter data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the Chapter was not found.</p>"
          }
        ]
      }
    },
    "group": "Chapter",
    "name": "GetChapter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ChapterController.php",
    "groupTitle": "Chapter"
  },
  {
    "type": "get",
    "url": "/chapter",
    "title": "1- Read all Chapter",
    "group": "Chapter",
    "name": "GetChapters",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ChapterController.php",
    "groupTitle": "Chapter"
  },
  {
    "type": "post",
    "url": "/chapter",
    "title": "3- Create a new Chapter",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha - unique .</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "verses",
            "description": "<p>require - integer .</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "place",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "sort",
            "description": "<p>integer .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"El Fatha\", \n  \"verses\": \"18\", \n  \"place\": \"Makka\", \n  \"sort\": \"13\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Chapter doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Chapter",
    "name": "PostChapter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ChapterController.php",
    "groupTitle": "Chapter"
  },
  {
    "type": "put",
    "url": "/chapter/:id",
    "title": "4- Change Chapter data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha - unique .</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "verses",
            "description": "<p>require - integer .</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "place",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "sort",
            "description": "<p>integer .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"El Fatha\", \n  \"verses\": \"18\", \n  \"place\": \"Makka\", \n  \"sort\": \"13\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModelNotFound",
            "description": "<p>The <code>id</code> of the Chapter was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Chapter doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Chapter",
    "name": "PutChapter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ChapterController.php",
    "groupTitle": "Chapter"
  },
  {
    "type": "delete",
    "url": "/chapter/:id",
    "title": "5- Delete Chapter data",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModalNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      }
    },
    "group": "Chapter",
    "name": "eleteReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ChapterController.php",
    "groupTitle": "Chapter"
  },
  {
    "type": "delete",
    "url": "/playlist/:id",
    "title": "5- Delete Playlist data",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModalNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      }
    },
    "group": "Playlist",
    "name": "DeleteReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/PlaylistController.php",
    "groupTitle": "Playlist"
  },
  {
    "type": "get",
    "url": "/playlist/:id",
    "title": "2- Show Playlist data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the Playlist was not found.</p>"
          }
        ]
      }
    },
    "group": "Playlist",
    "name": "GetPlaylist",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/PlaylistController.php",
    "groupTitle": "Playlist"
  },
  {
    "type": "get",
    "url": "/playlist",
    "title": "1- Read all Playlists",
    "group": "Playlist",
    "name": "GetPlaylists",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/PlaylistController.php",
    "groupTitle": "Playlist"
  },
  {
    "type": "post",
    "url": "/playlist",
    "title": "3- Create a new Playlist",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha - unique .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"MixSoundtracks\",  \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Playlist doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Playlist",
    "name": "PostPlaylist",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/PlaylistController.php",
    "groupTitle": "Playlist"
  },
  {
    "type": "put",
    "url": "/playlist/:id",
    "title": "4- Change Playlist data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha - unique .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"Primary SoundTracks\",  \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModelNotFound",
            "description": "<p>The <code>id</code> of the Playlist was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Playlist doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Playlist",
    "name": "PutPlaylist",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/PlaylistController.php",
    "groupTitle": "Playlist"
  },
  {
    "type": "delete",
    "url": "/reciter/:id",
    "title": "5- Delete Reciter data",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModalNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      }
    },
    "group": "Reciter",
    "name": "DeleteReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ReciterController.php",
    "groupTitle": "Reciter"
  },
  {
    "type": "get",
    "url": "/reciter/:id",
    "title": "2- Show Reciter data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the Reciter was not found.</p>"
          }
        ]
      }
    },
    "group": "Reciter",
    "name": "GetReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ReciterController.php",
    "groupTitle": "Reciter"
  },
  {
    "type": "get",
    "url": "/reciter",
    "title": "1- Read all Playlist",
    "group": "Reciter",
    "name": "GetReciters",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ReciterController.php",
    "groupTitle": "Reciter"
  },
  {
    "type": "post",
    "url": "/reciter",
    "title": "3- Create a new Reciter",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "image",
            "description": "<p>require - maxSize|3mb .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\": \"jhon\", \n  \"country\": \"UK\", \n  \"image\": \"mySmile.jpeg\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Reciter doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Reciter",
    "name": "PostReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ReciterController.php",
    "groupTitle": "Reciter"
  },
  {
    "type": "put",
    "url": "/reciter/:id",
    "title": "4- Change Reciter data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "image",
            "description": "<p>require - maxSize|3mb .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\": \"jhon\", \n  \"country\": \"UK\", \n  \"image\": \"mySmile.jpeg\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModelNotFound",
            "description": "<p>The <code>id</code> of the Reciter was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Reciter doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Reciter",
    "name": "PutReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/ReciterController.php",
    "groupTitle": "Reciter"
  },
  {
    "type": "get",
    "url": "/riwaya/:id",
    "title": "2- Show Riwaya data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the Riwaya was not found.</p>"
          }
        ]
      }
    },
    "group": "Riwaya",
    "name": "GetRiwaya",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/RiwayaController.php",
    "groupTitle": "Riwaya"
  },
  {
    "type": "get",
    "url": "/riwaya",
    "title": "1- Read all Riwaya",
    "group": "Riwaya",
    "name": "GetRiwayas",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/RiwayaController.php",
    "groupTitle": "Riwaya"
  },
  {
    "type": "post",
    "url": "/riwaya",
    "title": "3- Create a new Riwaya",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"hafs\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Riwaya doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Riwaya",
    "name": "PostRiwaya",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/RiwayaController.php",
    "groupTitle": "Riwaya"
  },
  {
    "type": "put",
    "url": "/riwaya/:id",
    "title": "4- Change Riwaya data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"hafs\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModelNotFound",
            "description": "<p>The <code>id</code> of the Riwaya was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Riwaya doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Riwaya",
    "name": "PutRiwaya",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/RiwayaController.php",
    "groupTitle": "Riwaya"
  },
  {
    "type": "delete",
    "url": "/riwaya/:id",
    "title": "5- Delete Riwaya data",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModalNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      }
    },
    "group": "Riwaya",
    "name": "eleteReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/RiwayaController.php",
    "groupTitle": "Riwaya"
  },
  {
    "type": "get",
    "url": "/track/:id",
    "title": "2- Show Track data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the Track was not found.</p>"
          }
        ]
      }
    },
    "group": "Track",
    "name": "GetTrack",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/TrackController.php",
    "groupTitle": "Track"
  },
  {
    "type": "get",
    "url": "/track",
    "title": "1- Read all Tracks",
    "group": "Track",
    "name": "GetTracks",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/TrackController.php",
    "groupTitle": "Track"
  },
  {
    "type": "post",
    "url": "/track",
    "title": "3- Create a new Track",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>require - alpha .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"Al Maouzatain\", \n  \"url\": \"http://server11.mp3quran.net/shatri/###.mp3\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Track doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Track",
    "name": "PostTrack",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/TrackController.php",
    "groupTitle": "Track"
  },
  {
    "type": "put",
    "url": "/track/:id",
    "title": "4- Change Track data",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>require - alpha .</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>require - alpha .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\": \"Al Maouzatain\", \n  \"url\": \"http://server11.islamweb.net/mashary/###.mp3\", \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModelNotFound",
            "description": "<p>The <code>id</code> of the Track was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ValidationError",
            "description": "<p>The fields of the new Track doesn't match our rules.</p>"
          }
        ]
      }
    },
    "group": "Track",
    "name": "PutTrack",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/TrackController.php",
    "groupTitle": "Track"
  },
  {
    "type": "delete",
    "url": "/track/:id",
    "title": "5- Delete Track data",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ModalNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      }
    },
    "group": "Track",
    "name": "eleteReciter",
    "description": "<p>no differences between versions of this api until now .</p>",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/app/Http/Controllers/TrackController.php",
    "groupTitle": "Track"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/public/docs/main.js",
    "group": "c__xampp_htdocs_zikrapp_backend_public_docs_main_js",
    "groupTitle": "c__xampp_htdocs_zikrapp_backend_public_docs_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "c:/xampp/htdocs/zikrapp-backend/public/template/main.js",
    "group": "c__xampp_htdocs_zikrapp_backend_public_template_main_js",
    "groupTitle": "c__xampp_htdocs_zikrapp_backend_public_template_main_js",
    "name": ""
  }
] });
