define({
  "name": "zikrapp Api",
  "version": "0.1.0",
  "description": "",
  "title": "Documetation",
  "url": "zikrapp.com:9000",
  "header": {
    "title": "info",
    "content": "<h2>Welcome to Zikrapp Api Documintation</h2>\n<p>This Documentaion helps you dealing with all API you need to achieve your Front-End ِِAِpplication\nand get your resources easily .</p>\n<h2>Official Repository</h2>\n<p>Repository for the Api can be found on the <a href=\"https://bitbucket.org/ahmedreo/zikrapp-backend\">zikrapp-backend</a>.</p>\n<h2>Security Vulnerabilities</h2>\n<p>If you discover a security vulnerability within API, please send an e-mail to Ahmed Yasser at ahmedreo4@gmail.com. All security vulnerabilities will be promptly addressed.</p>\n<!-- ## License\n\nThe Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT). -->\n"
  },
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-08-04T01:54:24.271Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
