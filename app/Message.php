<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Message extends Eloquent
{
    protected $fillable = [
        'body' , 'seen'
    ];

    public function sender()
    {
    	return $this->belongsTo('App\User' , 'sender_id');
    }
}
