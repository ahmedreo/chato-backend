<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSended extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $message; 
    public $recipient_id; 

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message , $recipient_id)
    {
        $this->message = $message;
        $this->recipient_id = $recipient_id;
        $this->recipient_id = $recipient_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['message-channel'];
    }
}
