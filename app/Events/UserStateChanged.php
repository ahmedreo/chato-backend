<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserStateChanged extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $auth_state;
    public $friend_ids;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($auth_state , $friend_ids)
    {
        $this->auth_state = $auth_state;
        $this->friend_ids = $friend_ids;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['user-channel'];
    }
}
