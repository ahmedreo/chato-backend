<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InvitationAccepted extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $for_sender;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($for_sender)
    {
        $this->for_sender = $for_sender;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['invitation-channel'];
    }
}
