<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    // protected $with = ['relations', 'invitations'];

    // protected $appends = ['username'];

    public function relations()
    {
        return $this->hasMany('App\Relation');
    }
    public function invitations()
    {
        return $this->hasMany('App\Invitation' , 'recipient_id')->where('accepted' , false);
    }
    public function messages_as_sender()
    {
        return $this->hasMany('App\Message' , 'sender_id');
    }
    public function messages_as_recipient()
    {
        return $this->hasMany('App\Message' , 'recipient_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Message' , 'recipient_id')->orderBy('created_at', 'desc')->take(5);
    }

}
