<?php

namespace App\Chato\Services\Validation;

class PlaylistValidator extends ValidateOrFail {

	public $rules = ['title' => 'required|alpha|unique:playlists'];

}