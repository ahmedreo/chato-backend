<?php

namespace App\Chato\Services\Validation;

use App\Chato\Exceptions\ValidationException;

use Validator;

abstract class ValidateOrFail {

	public function validate( array $data, array $rules = array(), array $custom_errors = array() ) {

		if ( empty( $rules ) && ! empty( $this->rules ) && is_array( $this->rules ) ) {

			$rules = $this->rules;
		}

		$validation = Validator::make( $data, $rules, $custom_errors );

		if ( $validation->fails() ) {

			throw new ValidationException( $validation->errors() );
		}

		return true;
	}

}