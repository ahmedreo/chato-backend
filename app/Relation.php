<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Relation extends Eloquent
{
	protected $fillable = ['friend_id','close'];

   	public function users()
    {
        return $this->belongsTo('App\User' , null , 'relation_ids', 'user_ids');
    }

    public function friend()
    {
        return $this->belongsTo('App\User' , 'friend_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Message' , 'message_id');
    }
}
