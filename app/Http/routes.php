<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//users
Route::resource('user', 'UserController');
Route::post('search' , 'SearchController@search');

// relations
Route::resource('relation' , 'RelationController');
Route::post('addCloseFriend' , 'RelationController@addCloseFriend');	
Route::post('deleteCloseFriend' , 'RelationController@deleteCloseFriend');	

//invitations
Route::resource('invitation' , 'InvitationController');
Route::post('acceptInvitation' , 'InvitationController@acceptInvitation');

//messages
Route::resource('message' , 'MessageController');	
Route::post('getMessages' , 'MessageController@getMessages');	
Route::post('readMessages' , 'MessageController@readMessages');	

// jwt auth

Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
Route::post('authenticate', 'AuthenticateController@authenticate');
Route::post('authenticate/user' , 'AuthenticateController@getAuthenticatedUser');	
Route::post('register' , 'AuthenticateController@register');	
Route::post('logout' , 'AuthenticateController@logout' );

// providers 

Route::post('auth/facebook', 'AuthenticateController@facebook');


