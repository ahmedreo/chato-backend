<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Relation;

use App\User;

use Auth;

class RelationController extends Controller
{
	public function __construct()
   	{
       $this->middleware('jwt.auth');
   	}
	public function auth()
	{
		return Auth::user();	
	}
	public function syncData()
	{
	 	return $this->auth()->relations()->with('friend')->get();
	}
	public function getOppositeRelation($relation)
	 {
	 	return $relation->friend()->first()->relations()->where('friend_id' , $this->auth()->id)->first();
	 } 
	public function index()
	{	
		return response()->json(['relations' => $this->syncData()] , 200);
	}
	public function destroy(Relation $relation)
	{
		$this->getOppositeRelation($relation)->delete();

		$relation->delete();

		return response()->json(['relations' => $this->syncData()] , 200);

	}
	public function addCloseFriend(Request $request)
	{
		$relation = Relation::find($request->relation_id);
		
		$relation->close = true;

		$relation->save();

		return response()->json(['relations' => $this->syncData()] , 200);
	}
	public function deleteCloseFriend(Request $request)
	{
		$relation = Relation::find($request->relation_id);
		
		$relation->close = false;

		$relation->save();

		return response()->json(['relations' => $this->syncData()] , 200);
	}
}
