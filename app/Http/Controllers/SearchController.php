<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SearchController extends Controller
{
    public function search(Request $request)
    {
    	$model = 'App\\' . ucfirst($request->get('model'));

    	$elq = new $model;

    	$attr = $request->get('attribute');

    	$query = $request->get('query');

    	$results = $elq->where($attr , 'like', '%'. $query .'%')->get();
        
		return response()->json(['results' => $results] , 200);
        
    }
}
