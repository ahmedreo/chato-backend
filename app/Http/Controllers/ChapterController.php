<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Chato\Services\Validation\ChapterValidator;

use App\Chapter;

class ChapterController extends Controller {

    /**
        * @api {get} /chapter 1- Read all Chapter
        * @apiGroup Chapter
        * @apiName GetChapters
        * @apiDescription no differences between versions of this api until now .
     */
    public function index()
    {
        return Chapter::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
        * @api {post} /chapter 3- Create a new Chapter
        * @apiParam {string} title require - alpha - unique .
        * @apiParam {string} verses require - integer .
        * @apiParam {file} place require - alpha .
        * @apiParam {file} sort  integer .
        * @apiParamExample {json} Request-Example:
        *     {
        *       "title": "El Fatha", 
        *       "verses": "18", 
        *       "place": "Makka", 
        *       "sort": "13", 
        *     }
        * @apiError ValidationError The fields of the new Chapter doesn't match our rules.
        * @apiGroup Chapter
        * @apiName PostChapter
        * @apiDescription no differences between versions of this api until now .
     */
    public function store(Request $request , ChapterValidator $chapterValidator )
    {       
            $chapterValidator->validate($request->all());

            Chapter::create([
                    'title' => $request->title,
                    'verses' => $request->verses,
                    'place' => $request->place,
                    'sort' => $request->sort,
                ]
            );

            return response()->json(['message' => 'chapter has created successfully'],200);
    }

    /**
        * @api {get} /chapter/:id 2- Show Chapter data
        * @apiParam {string} id Users-ID.
        * @apiError UserNotFound The <code>id</code> of the Chapter was not found.
        * @apiGroup Chapter
        * @apiName GetChapter
        * @apiDescription no differences between versions of this api until now .
     */
    public function show(Chapter $chapter)
    {
        return $chapter;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
        * @api {put} /chapter/:id 4- Change Chapter data
        * @apiParam {string} title require - alpha - unique .
        * @apiParam {string} verses require - integer .
        * @apiParam {file} place require - alpha .
        * @apiParam {file} sort  integer .
        * @apiParamExample {json} Request-Example:
        *     {
        *       "title": "El Fatha", 
        *       "verses": "18", 
        *       "place": "Makka", 
        *       "sort": "13", 
        *     }
        * @apiError ModelNotFound The <code>id</code> of the Chapter was not found.
        * @apiError ValidationError The fields of the new Chapter doesn't match our rules.
        * @apiGroup Chapter
        * @apiName PutChapter
        * @apiDescription no differences between versions of this api until now .
     */
    public function update(Request $request , Chapter $chapter , ChapterValidator $chapterValidator)
    {
        $chapterValidator->validate($request->all());

        $chapter->title = $request->title ;
        $chapter->verses = $request->verses ;
        $chapter->place = $request->place ;
        $chapter->sort = $request->sort ;

        $chapter->save();

        return response()->json(['message' => 'the chapter has successfully updated'] , 200);
    }

    /**
        * @api {delete} /chapter/:id 5- Delete Chapter data
        * @apiError ModalNotFound The <code>id</code> of the Chapter was not found.
        * @apiGroup Chapter
        * @apiName eleteReciter
        * @apiDescription no differences between versions of this api until now .
     */
    public function destroy(Chapter $chapter)
    {
        $chapter->delete();

        return response()->json(['message' => 'the chapter has successfully deleted'] , 200);
    }

}
