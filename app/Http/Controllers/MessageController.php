<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Events\MessageSended;

use App\Message;

use Auth;

class MessageController extends Controller
{
	public function __construct()
   	{
       $this->middleware('jwt.auth');
   	}
	public function auth()
	{
		return Auth::user();	
	}
   	public function sync_d_sender($recipient_id)
   	{
   		return $this->auth()->messages_as_sender()->where('recipient_id' , $recipient_id)->get();
   	}
   	public function sync_d_recipient($recipient_id)
   	{
   		return $this->auth()->messages_as_recipient()->where('sender_id' , $recipient_id)->get();
   	}
	public function getMessages(Request $request)
	{	
		return response()->json([
			'messages_as_sender' => $this->sync_d_sender($request->recipient_id),
			'messages_as_recipient' => $this->sync_d_recipient($request->recipient_id)
		]);
		
	}
	public function store(Request $request)
	{
		$auth = $this->auth();

		$message = new Message;

		$message->body = $request->body;

		$message->seen = false;
		
		$message->sender_id = $auth->_id;

		$message->recipient_id = $request->recipient_id;
		
		$message->save();

		event(new MessageSended($message->load('sender') , $request->recipient_id));

		return response()->json([
			'messages_as_sender' => $this->sync_d_sender($request->recipient_id),
		]);
		
	}
	public function readMessages(Request $request)
	{	

		Message::whereIn('_id', $request->messages_ids)->update(['seen' => true]);

		return response()->json(['messages' => $this->auth()->messages()->with('sender')->get()], 200);

	}	
}
