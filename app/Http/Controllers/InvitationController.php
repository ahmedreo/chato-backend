<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Events\InvitationCreated;

use App\Events\InvitationAccepted;

use App\Invitation;

use App\User;

use App\Relation;

use Auth;

use DB;

class InvitationController extends Controller
{
	public function __construct()
	{
		$this->middleware('jwt.auth');
	}
	public function auth()
	{
		return Auth::user();	
	}
	public function syncData_inv()
	{
		return $this->auth()->invitations()->where('accepted' , false)->with('sender')->get();
	}
	public function syncData_rel()
	{
		return $this->auth()->relations()->with('friend')->get();
	}
	public function index()
	{		
		return response()->json(['invitations' => $this->syncData_inv() ] ,200);
	}
	public function store(Request $request){

		$invitation = Invitation::forceCreate([
			'accepted' => false,
			'sender_id' => $this->auth()->id,
			'recipient_id' => $request->recipient_id
			]);

		$invitationWithSender = $invitation->with('sender')->find($invitation->_id)->toArray();

		event(new InvitationCreated($invitationWithSender));

		return response()->json(['message' => 'done'] , 200);
	}

	public function acceptInvitation(Request $request)
	{
		$invitation = Invitation::find($request->invitation_id);
		$invitation->accepted = true ;
		$invitation->save();

		return $this->reflectionFactory($invitation);
		
	}
	public function reflectionFactory($invitation)
	{	 	
		$relation_recipient =  $this->auth()->relations()->create([
			'friend_id' => $invitation->sender()->first()->_id,
			'close' => false
			])->_id;

		$relation_sender =  $invitation->sender()->first()->relations()->create([
			'friend_id' => $invitation->recipient()->first()->_id,
			'close' => false
			])->_id;

		$for_sender = Relation::with('friend')->find($relation_sender)->toArray(); 

		$for_recipient = Relation::with('friend')->find($relation_recipient)->toArray(); 

		event(new InvitationAccepted($for_sender));

		return response()->json(['relations' => $this->syncData_rel() , 'invitations' => $this->syncData_inv() ] , 200);
	}
	public function destroy(Invitation $invitation)
	{
		$invitation->delete();

		return response()->json(['invitations' => $this->syncData_inv()] , 200);
	}

}
