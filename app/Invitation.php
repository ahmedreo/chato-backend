<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Invitation extends Eloquent
{
    public function sender()
    {
    	return $this->belongsTo('App\User' , 'sender_id');
    }
    public function recipient()
    {
    	return $this->belongsTo('App\User' , 'recipient_id');
    }
}
