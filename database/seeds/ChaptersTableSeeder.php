<?php

use Faker\Factory as Facker ;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ChaptersTableSeeder extends seeder
{
	public function run()
	{
		$facker = Facker::create();

		foreach (range (1, 10) as $index) {

			App\Chapter::create([

				'title' => $facker->sentence($nbWords = 1, $variableNbWords = true),
				'verses' => $facker->numberBetween($min = 3, $max = 300),
				'place' => $facker->randomElement($array = array ('Medina','Mecca')),
				'sort' => $facker->unique()->numberBetween($min = 3, $max = 300)

			]);
		}
	}
}