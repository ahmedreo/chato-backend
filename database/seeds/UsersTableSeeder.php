<?php

use Faker\Factory as Facker ;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$facker = Facker::create();

    	$password = str_random(7);

        foreach (range (1,5) as $index)
        {
        	App\User::create([
				'username' => $facker->username,
				'email' => $facker->safeEmail,
				'password' => bcrypt($password),
				'test' => $password,
			]);
        }
    }
}
