<!DOCTYPE html>
<html>
    <head>
        <title>Zikrapp Api</title>

        <link href="https://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
                
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="animate.css">
       
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <script src="script.js"></script>

        <style>

            html, body {
                height: 100%;
                box-sizing: border-box;
                overflow-x: hidden; 
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
                border-top:15px solid black;

            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 130px;
            }
            .meter { 
            height: 1px;  /* Can be anything */
            position: relative;
            margin: 200px 0 20px 0; /* Just for demo spacing */
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            -webkit-box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
            -moz-box-shadow   : inset 0 -1px 1px rgba(255,255,255,0.3);
            box-shadow        : inset 0 -1px 1px rgba(255,255,255,0.3);
        }
        .meter > span {
            display: block;
            height: 100%;
               -webkit-border-top-right-radius: 8px;
            -webkit-border-bottom-right-radius: 8px;
                   -moz-border-radius-topright: 8px;
                -moz-border-radius-bottomright: 8px;
                       border-top-right-radius: 8px;
                    border-bottom-right-radius: 8px;
                -webkit-border-top-left-radius: 20px;
             -webkit-border-bottom-left-radius: 20px;
                    -moz-border-radius-topleft: 20px;
                 -moz-border-radius-bottomleft: 20px;
                        border-top-left-radius: 20px;
                     border-bottom-left-radius: 20px;
            background-color: black;
            position: relative;
            overflow: hidden;
        }
        
        .animate > span:after {
            display: none;
        }
        
        @-webkit-keyframes move {
            0% {
               background-position: 0 0;
            }
            100% {
               background-position: 50px 50px;
            }
        }
        
        .orange > span {
            background-color: #f1a165;
            background-image: -moz-linear-gradient(top, #f1a165, #f36d0a);
            background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f1a165),color-stop(1, #f36d0a));
            background-image: -webkit-linear-gradient(#f1a165, #f36d0a); 
        }
        
        .red > span {
            background-color: #f0a3a3;
            background-image: -moz-linear-gradient(top, #f0a3a3, #f42323);
            background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f0a3a3),color-stop(1, #f42323));
            background-image: -webkit-linear-gradient(#f0a3a3, #f42323);
        }
        
        .nostripes > span > span, .nostripes > span:after {
            -webkit-animation: none;
            background-image: none;
        }
        pre {
            background: black;
            text-align: left;
            padding: 20px;
            margin: 0 auto 30px auto; 
        }
        #page-wrap { width: 490px; margin: 80px auto; }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title animated fadeIn">Zikr Api</div>
            </div>
                <h2 class="animated fadeInRightBig"><i class="fa fa-database" style="margin-right: 10px"></i>   BackEnd-Developer: Ahmed Yasser</h2>
                <h2 class="animated fadeInLeftBig"><i class="fa fa-css3" style="margin-right: 10px"></i>   FrontEnd-Developer: Momen Yasser</h2>
                <h4 class="animated slideInDown">wait five seconeds to see the documentation .......</h4>
            <div class="meter">
                <span style="width: 100%"></span>
            </div>
        </div>
    </body>
</html>
